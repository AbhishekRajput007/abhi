//
//  LoginVC.swift
//  FoodTap
//
//  Created by Akash on 13/11/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButton_Action(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let Vc = storyboard.instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        navigationController?.pushViewController(Vc, animated: true)
    }
    
   
    @IBAction func backButton_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
