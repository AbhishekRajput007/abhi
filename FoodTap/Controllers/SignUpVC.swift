//
//  SignUpVC.swift
//  FoodTap
//
//  Created by Akash on 12/11/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

    @IBOutlet weak var createAccounttblview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension SignUpVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = self.createAccounttblview.dequeueReusableCell(withIdentifier: "CreateAccountCell", for: indexPath) as! CreateAccountCell
                return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
}
