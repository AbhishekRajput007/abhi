//
//  ViewController.swift
//  FoodTap
//
//  Created by Akash on 12/11/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func loginBtn(_ sender: Any) {
        if let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {

                      if let navigator = navigationController {
                          navigator.pushViewController(loginVC, animated: true)
                      }
                  }
    }
    
    @IBAction func createAccountClick(_ sender: Any) {
            if let signUp = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC {

                                 if let navigator = navigationController {
                                     navigator.pushViewController(signUp, animated: true)
                                 }
                            }
            
    }

    }
    

