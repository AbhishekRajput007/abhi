//
//  SideMenuVc.swift
//  FoodTap
//
//  Created by Abhishek Rajput on 05/12/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class SideMenuVc: UIViewController {
    @IBOutlet weak var profileBackView: UIView!
    @IBOutlet weak var profilePicImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        profileBackView.layer.cornerRadius = profileBackView.frame.size.height / 2
        profilePicImageView.layer.cornerRadius = profilePicImageView.frame.size.height / 2
    
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
