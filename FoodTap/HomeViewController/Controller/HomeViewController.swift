//
//  HomeViewController.swift
//  FoodTap
//
//  Created by Abhishek Rajput on 05/12/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit
import SideMenu
class HomeViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "HomeVcCell", bundle: nil), forCellReuseIdentifier: "HomeVcCell")
        
        tableView.reloadData()
        

    }
    
    @IBAction func sideDrawer_Action(_ sender: UIButton) {
        
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let Vc = storyboard.instantiateViewController(withIdentifier: "SideMenuNavigationController") as! SideMenuNavigationController
          present(Vc, animated: true, completion: nil)
        
    }
    

}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVcCell", for: indexPath) as! HomeVcCell
        return cell
    }
    
    
}
